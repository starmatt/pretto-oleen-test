import axios from 'axios';
import PropTypes from 'prop-types';
import { useState } from 'react';
import Loader from './Spinner';

export default function CalendarSlot({ slot }) {
  const [isLoading, setIsLoading] = useState(false);

  const postSlot = async () => {
    if (isLoading) {
      return;
    }

    setIsLoading(true);

    setTimeout(async () => {
      try {
        const response = await axios.post('/api/slot', { slot });

        console.log(response.data.success);
      } catch (e) {
        console.error(e);
      } finally {
        setIsLoading(false);
      }
    }, 1000);
  };

  return (
    <div className="px-4 mb-2">
      {slot.available ? (
        <button
          className="w-full h-100 bg-green-100 p-3 rounded hover:bg-green-200 active:bg-green-300"
          onClick={postSlot}
          disabled={isLoading}
        >
          {isLoading ? (
            <Loader className="w-6 h-6 text-white animate-spin fill-green-600 mx-auto" />
          ) : (
            slot.start
          )}
        </button>
      ) : (
        <div className="text-gray-300 py-3 px-5">—</div>
      )}
    </div>
  );
}

CalendarSlot.propTypes = {
  slot: PropTypes.object,
};
