<?php

$agenda = [
    [
        'name' => 'RDV #1',
        'start' => '2021-03-01T08:00:00Z',
        'end' => '2021-03-01T09:00:00Z',
    ],
    [
        'name' => 'RDV #2',
        'start' => '2021-03-01T14:00:00Z',
        'end' => '2021-03-01T19:15:00Z',
    ],
    [
        'name' => 'RDV #3',
        'start' => '2021-03-02T14:00:00Z',
        'end' => '2021-03-02T19:15:00Z',
    ],
    [
        'name' => 'RDV #4',
        'start' => '2021-03-04T14:00:00Z',
        'end' => '2021-03-05T19:15:00Z',
    ],
    [
        'name' => 'RDV #5',
        'start' => '2021-02-01T08:00:00Z',
        'end' => '2021-02-01T08:30:00Z',
    ],
];

$allowDayAndTime = [
    //
];

$newEvent = [
    'duration' => 15,
    'preparation' => 0,
    'from' => '2021-03-01T00:00:00Z',
    'to' => '2021-03-06T00:00:00Z',
];

// Create an array of time slots of a specified duration, from a time range
// starting at `$from` and ending at `$to`.
function generateSlots(int $from, int $to, int $duration = 30): array {
    $allowDurations = ['15', '30'];

    if (!in_array($duration, $allowDurations)) {
        throw new \Exception('Specified duration not allowed.');
    }

    $slots = [];

    for ($from; $from < $to; $from += $duration * 60) {
        $slots[] = [
            'day' => date('j', $from),
            'start' => $from,
            'end' => $from + $duration * 60,
            'available' => true,
        ];
    }

    return $slots;
}

// Loop through our slots to find conflicts with our 'events' array.
function filterSlots(array $slots, array $events): array {
    foreach ($events as $event) {
        foreach ($slots as $key => $slot) {
            if (
                $slot['start'] >= $event['start'] &&
                $slot['end'] <= $event['end']
            ) {
                $slots[$key]['available'] = false;
            }
        }
    }

    return $slots;
}

// Transform the slot timestamps into a readable format.
function slotTimestampsToString(array $slot, string $format = 'H:i:s'): array
{
    return [
        ...$slot,
        'start' => date($format, $slot['start']),
        'end' => date($format, $slot['end'])
    ];
}

try {
    $duration = $newEvent['duration'];
    $from = strtotime($newEvent['from']);
    $to = strtotime($newEvent['to']);

    // Create an array of planned events and convert their dates to timestamps.
    $events = array_map(fn ($event) => [
        'start' => strtotime($event['start']),
        'end' => strtotime($event['end'])
    ], $agenda);

    // Calculate the available slots.
    $slots = filterSlots(generateSlots($from, $to, $duration), $events);

    // Render our output more readable.
    $slots = array_map('slotTimestampsToString', $slots);

    // Output our slots.
    print_r($slots);
} catch (\Exception $exception) {
    echo $exception->getMessage() . PHP_EOL;
}

return 0;
