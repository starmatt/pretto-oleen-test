<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    /**
     * Send the calendar data.
     */
    public function getCalendar(Request $request, int $page = 1): JsonResponse
    {
        $duration = 30;

        $agenda = new \App\Services\Agenda([
            'duration' => $duration,
            'preparation' => 0,
            'from' => '2021-03-01T00:00:00Z',
            'to' => '2021-03-08T00:00:00Z',
        ]);

        return response()->json([
            'duration' => $duration,
            'dates' => $agenda->getDates(),
            'slots' => $agenda->paginate($page)->getSlots(),
        ]);
    }

    /**
     * Reserve a free timeslot in the calendar.
     */
    public function postSlot(Request $request): JsonResponse
    {
        return response()->json([
            'success' => true,
        ]);
    }
}
