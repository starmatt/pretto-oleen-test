import PropTypes from 'prop-types';

export default function CalendarHead({ dates, duration }) {
  return (
    <>
      <div className="flex justify-between bg-gray-50 font-bold text-center rounded-t">
        {!!dates.length &&
          dates.map((date) => (
            <div key={date.getTime()} className="flex-1 w-100 px-4 py-2">
              <div className="mb-1">
                {date.toLocaleDateString('fr-FR', {
                  weekday: 'short',
                })}

                <div>
                  {date.toLocaleDateString('fr-FR', {
                    month: '2-digit',
                    day: '2-digit',
                  })}
                </div>
              </div>
            </div>
          ))}
      </div>

      <div className="bg-gray-50 text-center py-5 border-y border-gray-300 text-sm">
        Durée du rendez-vous: {duration} minutes
      </div>
    </>
  );
}

CalendarHead.propTypes = {
  dates: PropTypes.arrayOf(PropTypes.instanceOf(Date)),
  duration: PropTypes.number,
};
