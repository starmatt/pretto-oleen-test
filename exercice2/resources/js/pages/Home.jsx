import Calendar from '../components/Calendar';

export default function Home() {
  return (
    <div className="flex min-h-screen w-100 bg-gray-300 items-center justify-center py-10">
      <div className="bg-white rounded p-6">
        <h1 className="font-bold text-3xl mb-2">Prenez rendez-vous</h1>

        <p className="text-base mb-5">
          Choisissez un créneau pour échanger par téléphone avec votre expert.
        </p>

        <Calendar />
      </div>
    </div>
  );
}
