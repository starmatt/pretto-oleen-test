import PropTypes from 'prop-types';
import CalendarSlot from './CalendarSlot';

export default function CalendarBody({ dates, slots }) {
  return (
    <div className="flex justify-between text-center rounded-t">
      {!!dates.length &&
        dates.map((date, index) => (
          <div
            key={date.getTime()}
            className={
              index !== dates.length - 1
                ? 'flex-1 min-w-0 pt-2 border-r border-gray-300'
                : 'flex-1 min-w-0 pt-2'
            }
          >
            {!!slots &&
              Object.keys(slots).map(
                (slotDate) =>
                  new Date(slotDate).getDate() === date.getDate() &&
                  slots[slotDate].map((slot) => (
                    <CalendarSlot slot={slot} key={slot.start} />
                  )),
              )}
          </div>
        ))}
    </div>
  );
}

CalendarBody.propTypes = {
  dates: PropTypes.arrayOf(PropTypes.instanceOf(Date)),
  slots: PropTypes.objectOf(PropTypes.array),
};
