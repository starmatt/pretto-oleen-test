import axios from 'axios';
import { useEffect, useState } from 'react';
import CalendarHead from './CalendarHead';
import CalendarBody from './CalendarBody';

export default function Calendar() {
  const [isLoading, setIsLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [duration, setDuration] = useState(null);
  const [dates, setDates] = useState([]);
  const [slots, setSlots] = useState(null);

  const fetchCalendar = async () => {
    if (isLoading) {
      return;
    }

    try {
      setIsLoading(true);

      const response = await axios.get(`/api/calendar/${currentPage}`);

      if (slots) {
        setSlots((slots) =>
          Object.keys(slots).reduce((mergedSlots, date) => {
            mergedSlots[date] = [...slots[date], ...response.data.slots[date]];

            return mergedSlots;
          }, {}),
        );
      } else {
        setSlots(response.data.slots);
      }

      setDuration(response.data.duration);
      setDates(response.data.dates.map((date) => new Date(date)));
      setCurrentPage(currentPage + 1);
    } catch (e) {
      console.error(e);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchCalendar();
  }, []);

  return (
    <>
      <div className="w-100 border border-gray-300 rounded mb-4">
        <CalendarHead dates={dates} duration={duration} />
        <CalendarBody dates={dates} slots={slots} />
      </div>

      <button
        className="border border-blue-500 hover:bg-blue-100 active:bg-blue-200 rounded-lg text-blue-500 font-semibold px-2 py-1"
        disabled={isLoading}
        onClick={fetchCalendar}
      >
        Afficher plus de créneaux
      </button>
    </>
  );
}
