<?php

namespace App\Services;

class Agenda
{
    private static $events = [
        [
            'name' => 'RDV #1',
            'start' => '2021-03-01T08:00:00Z',
            'end' => '2021-03-01T09:00:00Z',
        ],
        [
            'name' => 'RDV #2',
            'start' => '2021-03-01T14:00:00Z',
            'end' => '2021-03-01T19:15:00Z',
        ],
        [
            'name' => 'RDV #3',
            'start' => '2021-03-02T14:00:00Z',
            'end' => '2021-03-02T19:15:00Z',
        ],
        [
            'name' => 'RDV #4',
            'start' => '2021-03-04T14:00:00Z',
            'end' => '2021-03-05T19:15:00Z',
        ],
        [
            'name' => 'RDV #5',
            'start' => '2021-02-01T08:00:00Z',
            'end' => '2021-02-01T08:30:00Z',
        ],
    ];

    private array $allowedDurations = [15, 30];

    private int $slotDuration;

    private array $slots = [];

    public function __construct(array $newEvent)
    {
        $from = strtotime($newEvent['from']);
        $to = strtotime($newEvent['to']);

        if (! in_array($newEvent['duration'], $this->allowedDurations)) {
            throw new \Exception('Specified duration not allowed.');
        }

        $this->slotDuration = $newEvent['duration'];

        $slots = $this->filterSlots(
            $this->generateSlots($from, $to), array_map(fn ($event) => [
                'start' => strtotime($event['start']),
                'end' => strtotime($event['end']),
            ], static::$events)
        );

        $slots = array_map(
            fn ($slot) => $this->slotTimestampsToString($slot),
            $slots
        );

        $slots = array_reduce($slots, function ($carry, $slot) {
            $carry[$slot['date']][] = $slot;

            return $carry;
        }, []);

        $this->slots = $slots;
    }

    public function paginate($page): self
    {
        $perPage = 8;

        foreach ($this->slots as $date => $slots) {
            $this->slots[$date] = array_slice(
                $slots,
                $perPage * ($page - 1),
                $perPage
            );
        }

        return $this;
    }

    public function getDates(): array
    {
        return array_keys($this->slots);
    }

    public function getSlots(): array
    {
        return $this->slots;
    }

    private function generateSlots(int $from, int $to): array
    {
        $slots = [];

        for ($from; $from < $to; $from += $this->slotDuration * 60) {
            $end = $from + $this->slotDuration * 60;

            if (date('G', $from) >= '8' && date('G', $from) <= '18') {
                $slots[] = [
                    'date' => date('Y-m-d', $from),
                    'day' => date('j', $from),
                    'start' => $from,
                    'end' => $end,
                    'available' => true,
                ];
            }
        }

        return $slots;
    }

    private function filterSlots(array $slots, array $events): array
    {
        foreach ($events as $event) {
            foreach ($slots as $key => $slot) {
                if (
                    $slot['start'] >= $event['start'] &&
                        $slot['end'] <= $event['end']
                ) {
                    $slots[$key]['available'] = false;
                }
            }
        }

        return $slots;
    }

    private function slotTimestampsToString(
        array $slot,
        string $format = 'H:i'
    ): array {
        return [
            ...$slot,
            'start' => date($format, $slot['start']),
            'end' => date($format, $slot['end']),
        ];
    }
}
