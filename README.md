### Dépendences

- `php8`
- `composer`
- `yarn`

# Exercice 1

Génère un tableau dont chaque élément représente un créneau de 15 ou 30 minutes.

Pour lancer l'exercice 1 :

```sh
cd exercice1
php exercice1.php
```

Exemple de sortie:
```
Array
(
    [...]

    [15] => Array
        (
            [day] => 1
            [start] => 07:30:00
            [end] => 08:00:00
            [available] => 1
        )

    [16] => Array
        (
            [day] => 1
            [start] => 08:00:00
            [end] => 08:30:00
            [available] =>
        )

    [17] => Array
        (
            [day] => 1
            [start] => 08:30:00
            [end] => 09:00:00
            [available] =>
        )

    [18] => Array
        (
            [day] => 1
            [start] => 09:00:00
            [end] => 09:30:00
            [available] => 1
        )

    [19] => Array
        (
            [day] => 1
            [start] => 09:30:00
            [end] => 10:00:00
            [available] => 1
        )

    [...]
)
```

Pour modifier les critères d'entrée, il est possible d'éditer les tableaux
`$agenda` (qui représente les créneaux non-disponible) et `$newEvent` (qui
représente la plage de temps durant laquelle il est possible de trouver un
créneau libre) dans le fichier `exercice1.php`.

# Exercice 2

Pour lancer l'exercice 2 :

```sh
cd exercice2
composer install          # installation des dépendences PHP
yarn                      # installation des dépendences javascript
cp .env.example .env      # initialisation du fichier .env
php artisan key:generate  # création de la clé d'application

php artisan serve    # démarrer l'API sur un serveur local
yarn dev             # lancer la compilation des assets client (avec vite)
```

Puis ouvrir un navigateur à l'adresse `http://localhost:8000`.

Le code javascript se trouve dans le dossier [`./resources/js/`](https://gitlab.com/starmatt/pretto-oleen-test/-/tree/main/exercice2/resources/js).

Côté API, les fichiers intéressants à regarder sont:
- [`routes/api.php`](https://gitlab.com/starmatt/pretto-oleen-test/-/blob/main/exercice2/routes/api.php) (le fichier qui gère les routes d'api)
- [`app/Http/Controllers/CalendarController.php`](https://gitlab.com/starmatt/pretto-oleen-test/-/blob/main/exercice2/app/Http/Controllers/CalendarController.php) (le controlleur qui gère l'entrée et la sortie des requêtes HTTP)
- [`app/Services/Agenda.php`](https://gitlab.com/starmatt/pretto-oleen-test/-/blob/main/exercice2/app/Services/Agenda.php) (la classe qui implémente l'algo de l'exercice 1, avec quelques modifications)
